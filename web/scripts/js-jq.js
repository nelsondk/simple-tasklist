$(document).ready(function() {
    loadList();
});

$("#submit").click(function() {
    submitTask();
});

$("#delete").click(function() {
    var cb = document.getElementsByClassName("taskCB");
    for(var i = 0 ; i < document.taskListArea.length ; i++) {
        if(cb[i].checked) {
            var taskID = cb[i].value;
            $.post(
                "taskListServlet",
                {action:"deleteTask", taskID:taskID},
                function(data) {
                    printList(data);
                }
            );
        }
    }
});

$("#about").click(function() {
   alert("Simple Task List created with jQuery \nAuthor: Devin K. Nelson");
});

function printList(data) {
    $("#taskHolder").html(data);
    $(".taskCB").on('click', function() {
        $(this).closest('tr').toggleClass('marked');
    });
}

function loadList() {
    $.post(
        "taskListServlet",
        {action:"loadList"},
        function(data){
            printList(data);
        }
    );
}

function submitTask() {
    $taskInput = $("#taskInput").val();
    $taskDate = $("#taskDate").val();
    if($taskInput === "" || $taskInput === " ") {
        alert("You must enter a task");
        return false;
    }
    if($taskDate === "") {
        alert("You must enter a date");
        return false;
    }
    $.post(
        "taskListServlet", 
        {action:"submitTask", taskInput:$taskInput, taskDate:$taskDate}, 
        function(data){
            printList(data);
        }
    );
}

