<!DOCTYPE html>
<html>
    <head>
        <title>Simple Task Manager</title>
        <link href="styles/simple-task-jq.css" rel="stylesheet" />
        <link href="styles/myboot.css" rel="stylesheet" />
    </head>
    
    <body>
    <div class="container">
        <header class="row span12 offset2">
            <h1>Simple Task Manager - jQuery</h1>
        </header>
        <nav class="navbar">
            <div class="navbar-inner">
                <ul class="nav">
                    <li><a id="submit" href="#">Submit Task</a></li>
                    <li><a id="delete" href="#">Delete Task</a></li>
                    <li><a id="about" href="#">About</a></li>
                </ul>
            </div>
        </nav>
        <form id="inputArea" class="form-inline row span12" >
                <label for="taskInput"><h4>&gt;&gt;&gt; Task Name: </h4></label>
                <input id="taskInput" class="span4" type="text" />
                <label  class="offset1"><h4>Task Date</h4></label>
                <input type="date" id="taskDate"  />
        </form>
        <form id="taskListArea" name="taskListArea">
            <table class="table table-striped">
                <tbody id="taskHolder" class="row"></tbody>
            </table>
        </form>
    </div><!-- end container -->
    
    
        <script src="scripts/jquery-1.9.1.min.js"></script>
        <script src="scripts/js-jq.js"></script>
    </body>
</html>