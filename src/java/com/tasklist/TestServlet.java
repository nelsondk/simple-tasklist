package com.tasklist;

import java.io.IOException;
import java.io.PrintWriter;

import java.sql.Connection;
import java.sql.Statement;
import java.sql.ResultSet;
import java.sql.DriverManager;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class TestServlet extends HttpServlet {
    
    //create database credential variables
    private static final String DRIVER = "org.postgresql.Driver";
    private static final String URL = "jdbc:postgresql://localhost/testdb";
    private static final String USER = "dknadmin";
    private static final String PASS = "dknadmin";
    
    Connection con = null;
    Statement st = null;
    ResultSet rs = null;
    
    @Override
    public void init() {
        System.out.println("Initializing servlet...");
        System.out.println("Registering PostgreSQL Driver...");
        try {
            Class.forName(DRIVER);
        } catch (ClassNotFoundException ex) {
            ex.printStackTrace();
        }
    }
    
    @Override
    public void doPost(HttpServletRequest req, HttpServletResponse res) 
        throws ServletException, IOException{
        try {
            System.out.println("Creating connection to database...");
            con = DriverManager.getConnection(URL, USER, PASS);
            //Check if we are inserting or deleting a task
            if(req.getParameter("action").equals("submitTask")) {
                recordTask(req);
            } else if(req.getParameter("action").equals("deleteTask")) {
                deleteTask(req);
            } 
            
            //Reprint list no matter what    
            printList(res);
            
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
    
    public void printList(HttpServletResponse res) {
        try {
            System.out.println("Pulling records from the database...");
            System.out.println("Creating SELECT query...");
            String sql = "SELECT * FROM tasklist"
                    + " ORDER BY taskdate, taskid";
            st = con.createStatement();
            rs = st.executeQuery(sql);
            
            res.setContentType("text/plain");
            PrintWriter writer = res.getWriter();
            
            System.out.println("Printlng results from SELECT query...");
            while(rs.next()) {
                int taskID = rs.getInt("taskID");
                String taskText = rs.getString("taskText");
                String taskDate = rs.getString("taskDate");
                
                writer.println(
                    "<tr class=\"span12\">"
                    +"<td class=\"span1\"><input type=\"checkbox\" class=\"taskCB\" value=\"" + taskID + "\" /></td>"
                    + "<td class=\"span8\">" + taskText + "</td>"
                    + "<td class=\"span3\">" + taskDate + "</td>"
                    + "</tr>");
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
    
    public void recordTask(HttpServletRequest req) {
        String task = req.getParameter("taskInput");
        String date = req.getParameter("taskDate");
        
        System.out.println("Creating INSERT query...");
        try {
        String sql = "INSERT INTO tasklist (taskText, taskDate) VALUES "
            + "('" + task + "', '" + date + "')";
            st = con.createStatement();
            st.executeUpdate(sql);
        } catch(SQLException ex) {
            ex.printStackTrace();
        }    
    }
    
    public void deleteTask(HttpServletRequest req) {
        System.out.println("Creating DELETE query...");
        String taskID = req.getParameter("taskID");
        
        String sql = "DELETE FROM tasklist WHERE taskid = " + taskID;
        try {
            st = con.createStatement();
            st.executeUpdate(sql);
        } catch(SQLException ex) {
            ex.printStackTrace();
        }
    }
}